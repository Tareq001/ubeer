//
//  newProjectCell.swift
//  uBeer
//
//  Created by Domenico Varchetta on 21/02/2020.
//  Copyright © 2020 Tareq Saifullah. All rights reserved.
//

import UIKit

class newProjectCell: UICollectionViewCell {
    
    let plusLabel: UILabel = {
        let label = UILabel()
        label.text = "+"
        label.font = .boldSystemFont(ofSize: 70)
        label.textColor = .white
        return label
    }()
    
    let infoLabel: UILabel = {
        let label = UILabel()
        label.text = "New Project"
        label.font = .systemFont(ofSize: 18)
        label.textColor = .white
        return label
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        
        let newProjectStackView = UIStackView(arrangedSubviews: [
            plusLabel
        ])
        newProjectStackView.axis = .vertical
        newProjectStackView.alignment = .center
        newProjectStackView.distribution = .fill
        newProjectStackView.spacing = 0
        
        addSubview(newProjectStackView)
        newProjectStackView.fillSuperview()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
