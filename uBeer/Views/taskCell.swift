//
//  taskCellCollectionViewCell.swift
//  uBeer
//
//  Created by Domenico Varchetta on 02/03/2020.
//  Copyright © 2020 Tareq Saifullah. All rights reserved.
//

import UIKit

class taskCell: UICollectionViewCell {
    
    let taskTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Task Title"
        label.font = .boldSystemFont(ofSize: 30)
        label.textColor = .black
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    let taskInfoLabel: UILabel = {
        let label = UILabel()
        label.text = "Task Info"
        label.font = .systemFont(ofSize: 20)
        label.textColor = .black
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()
    
    let taskCheckImageView: UIImageView = {
        let iv = UIImageView()
        iv.widthAnchor.constraint(equalToConstant: 70).isActive = true
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
//    let taskCheckLabel: UILabel = {
//        let label = UILabel()
//        label.text = "Check"
//        label.font = .systemFont(ofSize: 18)
//        label.textColor = .black
//        label.textAlignment = .left
//        return label
//    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        let verticalStack = UIStackView(arrangedSubviews: [
            taskTitleLabel, taskInfoLabel
        ])
        verticalStack.axis = .vertical
        verticalStack.distribution = .fillProportionally
        verticalStack.spacing = 12
        
        let horizontalStack = UIStackView(arrangedSubviews: [
            verticalStack, taskCheckImageView
        ])
        horizontalStack.axis = .horizontal
        horizontalStack.distribution = .equalSpacing
        horizontalStack.spacing = 12
        
        addSubview(horizontalStack)
        horizontalStack.fillSuperview(padding: .init(top: 20, left: 20, bottom: 20, right: 20))
        
        heightAnchor.constraint(equalToConstant: 200).isActive = true
        widthAnchor.constraint(equalToConstant: frame.width).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
