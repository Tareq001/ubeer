//
//  TaskTableViewCell.swift
//  uBeer
//
//  Created by Tareq Saifullah on 27/02/2020.
//  Copyright © 2020 Tareq Saifullah. All rights reserved.
//

import UIKit

class TaskTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblWork: UILabel!
    @IBOutlet weak var lblAbout: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func configure(task:Task){
        lblWork.text = task.name
        lblAbout.text = task.info
    }
    
}
