//
//  TasksViewController.swift
//  uBeer
//
//  Created by Domenico Varchetta on 02/03/2020.
//  Copyright © 2020 Tareq Saifullah. All rights reserved.
//

import UIKit

class TasksViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var navBarTitle: UINavigationItem!
    @IBOutlet weak var collectionView: UICollectionView!
    
    let taskCellId = "projectTaskCell"
    var project: Project?
    var tasks = [Task]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tasks = project?.beer?.tasks?.sortedArray(using: [NSSortDescriptor(key: "created", ascending: true)]) as! [Task]
        
//        navBarTitle.title = "\(project?.name ?? "Project") Tasks"
//        navBar.barTintColor = .white
//        navBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        navBar.shadowImage = UIImage()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Ingredients", style: .done, target: self, action: #selector(addTapped))
        self.navigationItem.title = "\(project?.name ?? "Project") Tasks"
        
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "birraSfondo")
        backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
        view.insertSubview(backgroundImage, at: 0)
        
        collectionView.backgroundColor = .clear
        collectionView.register(taskCell.self, forCellWithReuseIdentifier: taskCellId)
        
        navBar.isHidden = true
    }
    
    @objc func addTapped(sender: UIBarButtonItem) {
        let ingredientsVC = self.storyboard?.instantiateViewController(identifier: "IngredientsViewController") as! IngredientsViewController
        ingredientsVC.callingType = 1
        ingredientsVC.tasksIngredients = project?.beer?.ingredients?.sortedArray(using: [NSSortDescriptor(key: "created", ascending: true)]) as? [Ingredient]
        self.present(ingredientsVC, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: taskCellId, for: indexPath) as! taskCell
        cell.taskTitleLabel.text = tasks[indexPath.item].name
        cell.taskInfoLabel.text = tasks[indexPath.item].info
        cell.taskCheckImageView.image = tasks[indexPath.item].isDone == true ? UIImage(named: "Checked") : UIImage(named:"unchecked")
        cell.layer.cornerRadius = 15
        cell.mask?.clipsToBounds = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if tasks[indexPath.item].isDone == false {
            tasks[indexPath.item].isDone = true
            
        } else {
            tasks[indexPath.item].isDone = false
        }
        collectionView.reloadItems(at: [indexPath])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return .init(width: view.frame.width - 50, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .init(top: 85, left: 0, bottom: 0, right: 0)
    }
    
}
