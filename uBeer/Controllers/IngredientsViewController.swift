//
//  IngredientsViewController.swift
//  uBeer
//
//  Created by Nadia Ruocco on 23/02/2020.
//  Copyright © 2020 Tareq Saifullah. All rights reserved.
//

import UIKit

class IngredientsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var tbList: UITableView!
    @IBAction func doneAction(_ sender: Any) {
        switch callingType {
            case 0:
                //        let pr = Project(name: projectName!, beer: selectedBeer!)
                
                
                //            Saving the Project in CoreData
                var projectDict = [String:String]()
                projectDict["name"] = projectName
                projectDict["beer"] = selectedBeer?.name
                projectDict["beerImage"] = selectedBeer?.image
                CoreDataQueries.shareInstance.saveProduct(obj: projectDict,ingredientObj:selectedBeer!.ingredients,taskObj:selectedBeer!.tasks)
                //     ---------------------------------
                
                let projects = CoreDataQueries.shareInstance.allProjects()
                delegate?.updateArray(projects:projects )
                self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
            default:
                print("Ammaccabanane")
        }
        
    }
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    var delegate: FirstViewDelegate? = nil
    var listItemArray: [String] = Array()
    var checkItemArray: [String] = Array()
    var selectedBeer: BeerType?
    var projectName: String?
    var tasksIngredients: [Ingredient]?// Fetched from CoreData
    var callingType: Int? // 0 porject creation - 1 project tasks
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch callingType {
            case 0:
                doneButton.setTitle("Done",for: .normal)
            default:
                doneButton.setTitle("",for: .normal)
        }
        
        let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
        backgroundImage.image = UIImage(named: "birraSfondo")
        backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
        view.insertSubview(backgroundImage, at: 0)
        
        tbList.contentInset = UIEdgeInsets(top: 85, left: 0, bottom: 0, right: 0)
        tbList.register(UINib.init(nibName:"TableViewCell", bundle: nil), forCellReuseIdentifier: "CheckListIdentifier")
        tbList.dataSource = self
        tbList.delegate = self
        tbList.separatorInset = .init(top: 50, left: 0, bottom: 50, right: 0)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch callingType {
            case 0:
                return (selectedBeer?.ingredients.count)!
            default:
                return tasksIngredients!.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckListIdentifier") as! TableViewCell
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        
        switch callingType {
            case 0:
                cell.lblTitle.text = selectedBeer?.ingredients[indexPath.row].name
                cell.lblCheck.image = (selectedBeer?.ingredients[indexPath.row].isChecked)! ? UIImage(named:"Checked" ) : UIImage(named: "unchecked")
            default:
                cell.lblTitle.text = tasksIngredients![indexPath.row].name
                cell.lblCheck.image = (tasksIngredients![indexPath.row].isChecked ? UIImage(named:"Checked" ) : UIImage(named: "unchecked"))
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        switch callingType {
            case 0:
                if selectedBeer?.ingredients[indexPath.row].isChecked == false {
                    selectedBeer?.ingredients[indexPath.row].isChecked = true
                } else {
                    selectedBeer?.ingredients[indexPath.row].isChecked = false
                }
            default:
                if tasksIngredients![indexPath.row].isChecked == false {
                    tasksIngredients![indexPath.row].isChecked = true
                } else {
                    tasksIngredients![indexPath.row].isChecked = false
                }
        }
        tbList.reloadRows(at: [indexPath], with: .automatic)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

