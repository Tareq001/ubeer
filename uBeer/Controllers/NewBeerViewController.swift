//
//  NewBeerViewController.swift
//  uBeer
//
//  Created by Nadia Ruocco on 03/03/2020.
//  Copyright © 2020 Tareq Saifullah. All rights reserved.
//

import UIKit

class NewBeerViewController: UIViewController, UICollectionViewDataSource,UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate  {
    
    @IBOutlet weak var newBeerCollectionView: UICollectionView!
    @IBOutlet weak var projectNameTextField: UITextField!
    @IBAction func cancelBtnTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func nextBtnTapped(_ sender: Any) {
        if selectedBeer == nil || projectNameTextField.text == "" {
            let alertController = UIAlertController(title: "Alert", message: "Missing project name or recipe", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) {
                (action: UIAlertAction!) in
                // Code in this block will trigger when OK button tapped.
                // No other code needed for this view
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion: nil)
        } else {
            

            
            let ingredientsVC = self.storyboard?.instantiateViewController(identifier: "IngredientsViewController") as! IngredientsViewController
            ingredientsVC.callingType = 0
            ingredientsVC.selectedBeer = selectedBeer
            ingredientsVC.projectName = projectNameTextField.text
            ingredientsVC.delegate = firstView as? FirstViewDelegate
            self.present(ingredientsVC, animated: true, completion: nil)
        }
    }
    
    var firstView: UIViewController?
    var selectedBeer: BeerType?
    var ingredientsModal = UIViewController()
    var beers: [BeerType] = [lager, belgian, stout, ipa, weisse]

        
        override func viewDidLoad() {
              super.viewDidLoad()
            
            self.navigationController?.navigationBar.barTintColor = .white
            self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
            self.navigationController?.navigationBar.shadowImage = UIImage()
            
            let backgroundImage = UIImageView(frame: UIScreen.main.bounds)
            backgroundImage.image = UIImage(named: "birraSfondo")
            backgroundImage.contentMode = UIView.ContentMode.scaleAspectFill
            view.insertSubview(backgroundImage, at: 0)
            
            newBeerCollectionView.backgroundColor = .clear
            
            newBeerCollectionView.register(BeerCollectionViewCell.self, forCellWithReuseIdentifier: "beercell")
    }
    
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return beers.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "beercell", for: indexPath) as! BeerCollectionViewCell
               
            cell.projectImageView.image = UIImage(named: beers[indexPath.item].image)
            cell.projectTitleLabel.text = beers[indexPath.item].name
            if selectedBeer?.name == beers[indexPath.item].name {
                cell.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
                cell.projectTitleLabel.textColor = .white
            } else {
                cell.backgroundColor = .white
                cell.projectTitleLabel.textColor = .black
            }
                cell.layer.cornerRadius = 15
                cell.layer.masksToBounds = true
                return cell
           }
        
    
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return .init(width: (newBeerCollectionView.frame.width / 3), height: (newBeerCollectionView.frame.height))
        }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedBeer = beers[indexPath.item]
        newBeerCollectionView.reloadData()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
        
}
